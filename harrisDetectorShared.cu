
// Based on CUDA SDK template from NVIDIA

// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

// includes, project
#include <cutil_inline.h>

#define max(a,b) (((a)>(b))?(a):(b))
#define min(a,b) (((a)<(b))?(a):(b))

// GPGPU block dimensions
int BLOCK_WIDTH;
int BLOCK_HEIGHT;

// harris detector code to run on the host
void harrisDetectorHost(unsigned int *h_idata, unsigned int w, unsigned int h,
                int ws,               // window size
                int threshold,        // threshold value to detect corners
                unsigned int * reference)
{
    int i,j,k,l;  // indexes in image
    int Ix, Iy;   // gradient in XX and YY
    int R;        // R metric
    int sumIx2, sumIy2, sumIxIy;

    for(i=0; i<h; i++) //height image
    {
        for(j=0; j<w; j++) //width image
        {
            reference[i*w+j]=h_idata[i*w+j]/4; // to obtain a faded background image
        }
    }

    for(i=ws+1; i<h-ws; i++) //height image
    {
        for(j=ws+1; j<w-ws; j++) //width image
        {
           sumIx2=0.0;sumIy2=0.0;sumIxIy=0.0;
           for(k=-ws; k<=ws; k++) //height window
              {
                  for(l=-ws; l<=ws; l++) //width window
                  {
                        Ix = ((int)h_idata[(i+k-1)*w + j+l] - (int)h_idata[(i+k)*w + j+l])/32;
                        Iy = ((int)h_idata[(i+k)*w + j+l-1] - (int)h_idata[(i+k)*w + j+l])/32;
			sumIx2 += Ix*Ix;
			sumIy2 += Iy*Iy;
			sumIxIy += Ix*Iy;
                  }
              }

              R = sumIx2*sumIy2-sumIxIy*sumIxIy-0.05*(sumIx2+sumIy2)*(sumIx2+sumIy2);
              if(R > threshold) {
                   reference[i*w+j]=255;
              }
        }
    }
}

// to obtain a faded background image
__global__ void fadedBackground(unsigned int *h_idata, unsigned int w, unsigned int h,
                  unsigned int * h_odata)
{
	// indexes in image
	int j = threadIdx.x + blockIdx.x * blockDim.x;
	int i = threadIdx.y + blockIdx.y * blockDim.y;

    int index=i*w+j;

	// if pixel is in range
	if ((i < h) && (j < w))
	{
		h_odata[index]=h_idata[index]/4; // to obtain a faded background image
	}
}

// harris detector kernel to run on the GPU
__global__ void harrisDetectorKernel(unsigned int *h_idata, unsigned int w, unsigned int h,
                  int ws,               // window size
                  int threshold,        // threshold value to detect corners
                  int block_width,      // effective width
                  int block_height,     // efefctive height
                  unsigned int * h_odata)
{
    // shared memory
    extern __shared__ int subimg[];

    // indexes in image
	int j = threadIdx.x + blockIdx.x * block_width;
	int i = threadIdx.y + blockIdx.y * block_height;
	int row = threadIdx.x;
    int col = threadIdx.y;
    int blkw = blockDim.x;
    int blkh = blockDim.y;
	int k, l;
	int Ix, Iy;   // gradient in XX and YY
    int R;        // R metric
    int sumIx2, sumIy2, sumIxIy;

    // global and shared memory indexes
    int shpos=col*blkw+row;
	int glpos=i*w+j;

    if ((i < h) && (j < w))
	{
		subimg[shpos]=h_idata[glpos];
    }

    __syncthreads();

    if ((i>=ws+1) && (i<h-ws) && (j>=ws+1) && (j<w-ws))
	{
        if ((col>=ws+1) && (col<blkh-ws) && (row>=ws+1) && (row<blkw-ws))
        {
            sumIx2=0.0;sumIy2=0.0;sumIxIy=0.0;
            for (k=-ws; k<=ws; k++) //height window
            {
                for (l=-ws; l<=ws; l++) //width window
                {
                    Ix = ((int)subimg[(col+k-1)*blkw + row+l] - (int)subimg[(col+k)*blkw + row+l])/32;
                    Iy = ((int)subimg[(col+k)*blkw + row+l-1] - (int)subimg[(col+k)*blkw + row+l])/32;
                    sumIx2 += Ix*Ix;
                    sumIy2 += Iy*Iy;
                    sumIxIy += Ix*Iy;
                }
            }

            R = sumIx2*sumIy2-sumIxIy*sumIxIy-0.05*(sumIx2+sumIy2)*(sumIx2+sumIy2);
            if (R > threshold)
            {
                h_odata[glpos]=255;
            }
        }
	}
}

// harris detector code to run on the GPU
void harrisDetectorDevice(unsigned int *h_idata, unsigned int w, unsigned int h,
                  int ws,               // window size
                  int threshold,        // threshold value to detect corners
                  unsigned int * h_odata)
{
    // Load idata to device memory
	unsigned int *i_image;
	size_t size = w * h * sizeof(unsigned int);
	cudaMalloc(&i_image, size);
	cudaMemcpy(i_image, h_idata, size, cudaMemcpyHostToDevice);

	// Allocate odata in device memory
	unsigned int *o_image;
	cudaMalloc(&o_image, size);

	// Invoke kernel
	unsigned int n_block_width = (w + BLOCK_WIDTH - 1) / BLOCK_WIDTH;
	unsigned int n_block_height = (h + BLOCK_HEIGHT - 1) / BLOCK_HEIGHT;
	dim3 dimBlock(BLOCK_WIDTH, BLOCK_HEIGHT);
	dim3 dimGrid(n_block_width, n_block_height);
	fadedBackground<<<dimGrid, dimBlock>>>(i_image, w, h, o_image);

	// Invoke kernel
    n_block_width = (w + BLOCK_WIDTH - 1) / BLOCK_WIDTH;
	n_block_height = (h + BLOCK_HEIGHT - 1) / BLOCK_HEIGHT;
	dim3 dimBlockSub(BLOCK_WIDTH + ws * 2 + 1, BLOCK_HEIGHT + ws * 2 + 1);
	dim3 dimGridSub(n_block_width, n_block_height);
	unsigned int sharedMemSize = (BLOCK_WIDTH + 2 * ws + 1) * (BLOCK_HEIGHT + 2 * ws + 1) * sizeof(unsigned int);
	harrisDetectorKernel<<<dimGridSub, dimBlockSub, sharedMemSize>>>(i_image, w, h, ws, threshold, BLOCK_WIDTH, BLOCK_HEIGHT, o_image);

	// Copy result from device memory to host memory
	cudaMemcpy(h_odata, o_image, size, cudaMemcpyDeviceToHost);

	// Free device memory
	cudaFree(i_image);
	cudaFree(o_image);
}

// print command line format
void usage(char *command)
{
    printf("Usage: %s [-h] [-d device] [-i inputfile] [-o outputfile] [-r referenceFile] [-w windowsize] [-t threshold] [-b blocksize]\n",command);
}

// main
int main( int argc, char** argv)
{

    // default command line options
    int deviceId = 0;
    char *fileIn=(char *)"chess.pgm",*fileOut=(char *)"chessOut.pgm",*referenceOut=(char *)"reference.pgm";
    int ws = 2, threshold = 500;
    BLOCK_WIDTH = 8;
    BLOCK_HEIGHT = 8;

    // parse command line arguments
    int opt;
    while( (opt = getopt(argc,argv,"d:i:o:r:w:t:b:h")) !=-1)
    {
        switch(opt)
        {

            case 'd':
                if(sscanf(optarg,"%d",&deviceId)!=1)
                {
                    usage(argv[0]);
                    exit(1);
                }
                break;

            case 'i':
                if(strlen(optarg)==0)
                {
                    usage(argv[0]);
                    exit(1);
                }

                fileIn = strdup(optarg);
                break;
            case 'o':
                if(strlen(optarg)==0)
                {
                    usage(argv[0]);
                    exit(1);
                }
                fileOut = strdup(optarg);
                break;
            case 'r':
                if(strlen(optarg)==0)
                {
                    usage(argv[0]);
                    exit(1);
                }
                referenceOut = strdup(optarg);
                break;
            case 'w':
                if(strlen(optarg)==0 || sscanf(optarg,"%d",&ws)!=1)
                {
                    usage(argv[0]);
                    exit(1);
                }
                break;
            case 't':
                if(strlen(optarg)==0 || sscanf(optarg,"%d",&threshold)!=1)
                {
                    usage(argv[0]);
                    exit(1);
                }
                break;
            case 'b':
                if(strlen(optarg)==0 || sscanf(optarg,"%d",&BLOCK_WIDTH)!=1)
                {
                    usage(argv[0]);
                    exit(1);
                }
                BLOCK_HEIGHT = BLOCK_WIDTH;
                break;
            case 'h':
                usage(argv[0]);
                exit(0);
                break;

        }
    }

    // select cuda device
    cutilSafeCall( cudaSetDevice( deviceId ) );

    // create events to measure host harris detector time and device harris detector time

    cudaEvent_t startH, stopH, startD, stopD;
    cudaEventCreate(&startH);
    cudaEventCreate(&stopH);
    cudaEventCreate(&startD);
    cudaEventCreate(&stopD);



    // allocate host memory
    unsigned int* h_idata=NULL;
    unsigned int h,w;

    //load pgm
    if (cutLoadPGMi(fileIn, &h_idata, &w, &h) != CUTTrue) {
        printf("Failed to load image file: %s\n", fileIn);
        exit(1);
    }

    // allocate mem for the result on host side
    unsigned int* h_odata = (unsigned int*) malloc( h*w*sizeof(unsigned int));
    unsigned int* reference = (unsigned int*) malloc( h*w*sizeof(unsigned int));

    // detect corners at host

    cudaEventRecord( startH, 0 );
    harrisDetectorHost(h_idata, w, h, ws, threshold, reference);
    cudaEventRecord( stopH, 0 );
    cudaEventSynchronize( stopH );

    // detect corners at GPU
    cudaEventRecord( startD, 0 );
    harrisDetectorDevice(h_idata, w, h, ws, threshold, h_odata);
    cudaEventRecord( stopD, 0 );
    cudaEventSynchronize( stopD );

    // check if kernel execution generated and error
    cutilCheckMsg("Kernel execution failed");

    float timeH, timeD;
    cudaEventElapsedTime( &timeH, startH, stopH );
    printf( "Host processing time: %f (ms)\n", timeH);
    cudaEventElapsedTime( &timeD, startD, stopD );
    printf( "Device processing time: %f (ms)\n", timeD);

    // save output images
    if (cutSavePGMi(referenceOut, reference, w, h) != CUTTrue) {
        printf("Failed to save image file: %s\n", referenceOut);
        exit(1);
    }
    if (cutSavePGMi(fileOut, h_odata, w, h) != CUTTrue) {
        printf("Failed to save image file: %s\n", fileOut);
        exit(1);
    }

    // cleanup memory
    cutFree( h_idata);
    free( h_odata);
    free( reference);

    cutilDeviceReset();
}

