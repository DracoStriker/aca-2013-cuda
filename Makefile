
TARGETS = lib/libcutil_x86_64.a harrisDetector global optimized super shared texture

all: $(TARGETS)

harrisDetector: harrisDetector.cu
	nvcc -arch=sm_13 -O3 -Icommon/inc harrisDetector.cu -Llib -lcutil_x86_64 -o harrisDetector

global: harrisDetectorGlobal.cu
	nvcc -arch=sm_13 -O3 -Icommon/inc harrisDetectorGlobal.cu -Llib -lcutil_x86_64 -o harrisDetectorGlobal

optimized: harrisDetectorGlobalOptimized.cu
	nvcc -arch=sm_13 -O3 -Icommon/inc harrisDetectorGlobalOptimized.cu -Llib -lcutil_x86_64 -o harrisDetectorGlobalOptimized

super: harrisDetectorGlobalSuperOptimized.cu
	nvcc -arch=sm_13 -O3 -Icommon/inc harrisDetectorGlobalSuperOptimized.cu -Llib -lcutil_x86_64 -o harrisDetectorGlobalSuperOptimized

shared: harrisDetectorShared.cu
	nvcc -arch=sm_13 -O3 -Icommon/inc harrisDetectorShared.cu -Llib -lcutil_x86_64 -o harrisDetectorShared

texture: harrisDetectorTexture.cu
	nvcc -arch=sm_13 -O3 -Icommon/inc harrisDetectorTexture.cu -Llib -lcutil_x86_64 -o harrisDetectorTexture

lib/libcutil_x86_64.a:
	make -C common

clean:
	make -C common clean
	rm -f $(TARGETS)
